#include <ctype.h>
#include <stdio.h>
#include "tj_tokenize.h"
#include "tj_error.h"

#define is_ident(_char) ((_char) == '_' || isalpha((_char)))
#define is_ident_inside(_char) (is_ident(_char) || isdigit((_char)))

tj_token tj_get_token(tj_tokenize_state* state)
{
    while (isspace(state->code.string[state->position]))
        state->position++;

    state->last_position = state->position;
    size_t start = state->position;

    if (is_ident(state->code.string[state->position]))
    {
        while (is_ident_inside(state->code.string[state->position]))
            state->position++;

        tj_token token = tj_make_token(tj_token_ident,
                                           tj_make_location(start, state->position));

        tj_string_view value = tj_get_token_value(state->code, token);
        if (tj_string_view_equal(value, tj_sv("proc")))
            token.type = tj_token_proc;
        else if (tj_string_view_equal(value, tj_sv("var")))
            token.type = tj_token_var;
        else if (tj_string_view_equal(value, tj_sv("if")))
            token.type = tj_token_if;
        else if (tj_string_view_equal(value, tj_sv("else")))
            token.type = tj_token_else;
        else if (tj_string_view_equal(value, tj_sv("while")))
            token.type = tj_token_while;
        else if (tj_string_view_equal(value, tj_sv("ret")))
            token.type = tj_token_ret;

        return token;
    }
    else if (isdigit(state->code.string[state->position]))
    {
        while (isdigit(state->code.string[state->position]))
            state->position++;
        return tj_make_token(tj_token_number,
                               tj_make_location(start, state->position));
    }
    else if (state->code.string[state->position] == '/' &&
             state->code.string[state->position + 1] == '/')
    {
        while (state->code.string[state->position] != '\n')
            state->position++;
        return tj_get_token(state);
    }
    else switch (state->code.string[state->position])
    {
        case '(':
        {
            state->position++;
            return tj_make_token(tj_token_paren_open,
                                 tj_make_location(start, state->position));
        }
        
        case ')':
        {
            state->position++;
            return tj_make_token(tj_token_paren_close,
                                 tj_make_location(start, state->position));
        }
        
        case ',':
        {
            state->position++;
            return tj_make_token(tj_token_comma,
                                 tj_make_location(start, state->position));
        }
        
        case '{':
        {
            state->position++;
            return tj_make_token(tj_token_curly_open,
                                 tj_make_location(start, state->position));
        }

        case '}':
        {
            state->position++;
            return tj_make_token(tj_token_curly_close,
                                 tj_make_location(start, state->position));
        }

        case ';':
        {
            state->position++;
            return tj_make_token(tj_token_semicolon,
                                 tj_make_location(start, state->position));
        }

        case '*':
        {
            state->position++;
            return tj_make_token(tj_token_star,
                                 tj_make_location(start, state->position));
        }

        case '=':
        {
            state->position++;
            return tj_make_token(tj_token_equal,
                                 tj_make_location(start, state->position));
        }
        
        case '\0':
            return tj_make_token(tj_token_eof,
                                   tj_make_location(start, state->position));
    }

    tj_error("unexpected character '%c'\n", state->code.string[state->position]);
    return tj_make_token(tj_token_err, tj_make_location(state->position, state->position));
}

tj_string_view tj_get_token_value(tj_string_view code, tj_token token)
{
    return tj_slice(code, token.location.start, token.location.end);
}

void tj_backtrack(tj_tokenize_state* state)
{
    state->position = state->last_position;
}
