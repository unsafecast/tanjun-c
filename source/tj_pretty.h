#ifndef tj_pretty_h
#define tj_pretty_h
#include <stdio.h>
#include "tj_token.h"
#include "tj_ast.h"

void tj_pretty_token(FILE* file, tj_token token, tj_string_view code);
void tj_pretty_ast_node(FILE* file, tj_ast_node* node, tj_string_view code, int indent_level);

#endif