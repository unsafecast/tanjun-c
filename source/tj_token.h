#ifndef tj_token_h
#define tj_token_h
#include "tj_location.h"
#include "tj_string.h"
#include "tj_dynamic_array.h"

typedef enum
{
    tj_token_err = -99,

    tj_token_none = 0,
    tj_token_ident,
    tj_token_number,

    tj_token_paren_open,
    tj_token_paren_close,
    tj_token_comma,
    tj_token_curly_open,
    tj_token_curly_close,
    tj_token_semicolon,
    tj_token_star,
    tj_token_equal,

    tj_token_proc,
    tj_token_var,
    tj_token_if,
    tj_token_else,
    tj_token_while,
    tj_token_ret,

    tj_token_eof,
} tj_token_type;

typedef struct
{
    tj_token_type type;
    tj_location location;
} tj_token;
typedef tj_dynamic_array(tj_token) tj_token_array;

#define tj_make_token(_type, _location) ((tj_token){.type = (_type), .location = (_location)})

#endif