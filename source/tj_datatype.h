#ifndef tj_datatype_h
#define tj_datatype_h
#include <stdio.h>
#include "tj_string.h"

typedef enum
{
    tj_datatype_type_none = 0,
    tj_datatype_type_ptr,
    tj_datatype_type_int,
    tj_datatype_type_nothing,
    tj_datatype_type_user_defined,
} tj_datatype_type;

typedef struct
{
    tj_datatype_type type;
} tj_datatype;

typedef struct
{
    tj_datatype base;
    tj_datatype* target;
} tj_datatype_ptr;

typedef struct
{
    tj_datatype base;
    int bit_count;  // 0 if not yet decided
} tj_datatype_int;

typedef struct
{
    tj_datatype base;
    tj_string_view name;
} tj_datatype_user_defined;

void tj_print_datatype(FILE* file, tj_datatype* type);

#endif
