#ifndef tj_error_h
#define tj_error_h

// TODO: Remake with _Generic
// TODO: Add more information like location and file

void tj_error(const char* fmt, ...);
void tj_warning(const char* fmt, ...);
void tj_hint(const char* fmt, ...);

#endif