#include <stdlib.h>
#include "tj_parse.h"
#include "tj_error.h"

static tj_ast_node* error_node()
{
    tj_ast_node* node = calloc(1, sizeof(tj_ast_node));
    node->type = tj_ast_node_err;

    return (tj_ast_node*)node;
}

static tj_ast_node* node_eof(tj_token token)
{
    tj_ast_node* node = calloc(1, sizeof(tj_ast_node));
    node->type = tj_ast_node_eof;
    node->location = token.location;

    return node;
}

#define handle_errors_for_token(_token)\
    do { if ((_token).type < 1) return error_node(); } while (0)

#define token_expect(_token, _type, _fmt, ...) do { if ((_token).type != (_type))\
    { tj_error(_fmt, __VA_ARGS__); return error_node(); }} while (0)

#define token_get_and_expect(_state, _type, _fmt, ...)\
    do { tj_token token = tj_get_token((_state));\
         handle_errors_for_token(token);\
         token_expect(token, (_type), _fmt, __VA_ARGS__); } while (0)

#define handle_errors_for_node(_node)\
    do { if ((_node)->type < 1) return (tj_ast_node*)(_node); } while (0)

#define make_node(_type, _enum_type, _location, ...)\
    ({ _type* node = calloc(1, sizeof(_type));\
        *node = (_type)__VA_ARGS__;\
        node->node.type = (_enum_type);\
        node->node.location = (_location);\
        (tj_ast_node*)node; })

static bool get_and_match(tj_tokenize_state* state, tj_token_type type)
{
    tj_token token = tj_get_token(state);
    return token.type == type;
}

static bool match(tj_tokenize_state* state, tj_token_type type)
{
    bool result = get_and_match(state, type);
    tj_backtrack(state);

    return result;
}

static tj_ast_node* parse_simplest_expression(tj_tokenize_state* state)
{
    tj_token token = tj_get_token(state);
    handle_errors_for_token(token);

    if (token.type == tj_token_ident)
    {
        return make_node(tj_ast_symbol, tj_ast_node_symbol, token.location,
                        {.symbol=tj_get_token_value(state->code, token)});
    }

    if (token.type == tj_token_number)
    {
        return make_node(tj_ast_number, tj_ast_node_number, token.location,
                         {.number=tj_get_token_value(state->code, token)});
    }

    tj_string_view str = tj_get_token_value(state->code, token);
    tj_error("expected an expression, got '%.*s'\n", str.size, str.string);
    return error_node();
}

static tj_ast_node* finish_parse_call(tj_tokenize_state* state, tj_ast_node* name)
{
    token_get_and_expect(state, tj_token_paren_open, "expected '(' at the beginning of a function call\n");
    tj_ast_node_array args;
    tj_init_dynamic_array(tj_ast_node*, args);
    
    if (match(state, tj_token_paren_close))
    {
        handle_errors_for_token(tj_get_token(state));
        return make_node(tj_ast_funcall, tj_ast_node_funcall, name->location,
                         {.name = name, .arguments = args});
    }
    
    do
    {
        tj_ast_node* expr = tj_parse_expression(state);
        handle_errors_for_node(expr);
        tj_push(args, expr);
    } while (get_and_match(state, tj_token_comma));
    tj_backtrack(state);
    token_get_and_expect(state, tj_token_paren_close, "expected ')' after function call\n");

    return make_node(tj_ast_funcall, tj_ast_node_funcall, name->location,
                     {.name = name, .arguments = args});
}

static tj_ast_node* parse_call_or_(tj_tokenize_state* state)
{
    tj_ast_node* expr = parse_simplest_expression(state);
    handle_errors_for_node(expr);

    while (1)
    {
        if (match(state, tj_token_paren_open))
        {
            expr = finish_parse_call(state, expr);
        }
        // Here we're also gonna match struct access & stuff like this
        else
        {
            break;
        }
    }

    return expr;
}

static tj_ast_node* parse_body(tj_tokenize_state* state);

static tj_ast_node* parse_if(tj_tokenize_state* state)
{
    token_get_and_expect(state, tj_token_if, "expected 'if' at the beginning of if expression\n");

    tj_ast_node* condition = tj_parse_expression(state);
    handle_errors_for_node(condition);

    tj_ast_node* body = parse_body(state);
    handle_errors_for_node(body);
    
    tj_ast_node* else_body = 0;
    if (match(state, tj_token_else))
    {
        tj_get_token(state);
        else_body = parse_body(state);
    }

    return make_node(tj_ast_if, tj_ast_node_if, condition->location,
                     {.condition=condition, .if_body=(tj_ast_body*)body,
                      .else_body=(tj_ast_body*)else_body});
}

tj_ast_node* tj_parse_expression(tj_tokenize_state* state)
{
    return parse_call_or_(state);
}

tj_datatype* parse_inner_datatype(tj_tokenize_state* state)
{
#define int_of(_bits) ({ tj_datatype_int* i = calloc(1, sizeof(tj_datatype_int));\
                         i->base.type = tj_datatype_type_int;\
                         i->bit_count = (_bits); (tj_datatype*)i;})
    if (match(state, tj_token_star))
    {
        tj_get_token(state);
        tj_datatype_ptr* type = calloc(1, sizeof(tj_datatype_ptr));
        type->base.type = tj_datatype_type_ptr;
        type->target = parse_inner_datatype(state);
        return (tj_datatype*)type;
    }
    
    if (match(state, tj_token_ident))
    {
        tj_token token = tj_get_token(state);
        
        tj_string_view val = tj_get_token_value(state->code, token);
        if (tj_string_view_equal(val, tj_sv("Int8"))) 
        {
            return int_of(8);
        }
        if (tj_string_view_equal(val, tj_sv("Int16"))) 
        {
            return int_of(16);
        }
        if (tj_string_view_equal(val, tj_sv("Int32"))) 
        {
            return int_of(32);
        }
        if (tj_string_view_equal(val, tj_sv("Int64"))) 
        {
            return int_of(64);
        }
        if (tj_string_view_equal(val, tj_sv("Nothing")))
        {
            tj_datatype* type = calloc(1, sizeof(tj_datatype));
            type->type = tj_datatype_type_nothing;
            return type;
        }
        else
        {
            tj_datatype_user_defined* type = calloc(1, sizeof(tj_datatype_user_defined));
            type->base.type = tj_datatype_type_user_defined;
            type->name = tj_get_token_value(state->code, token);
            return (tj_datatype*)type;
        }
    }
    
    return 0;
#undef int_of
}

tj_ast_node* parse_datatype(tj_tokenize_state* state)
{
    tj_token token = tj_get_token(state);
    tj_backtrack(state);
    tj_datatype* type = parse_inner_datatype(state);
    
    if (type == 0)
    {
        tj_string_view val = tj_get_token_value(state->code, token);
        tj_error("expected type, found '%.*s'\n", val.size, val.string);
        return error_node();
    }
    
    return make_node(tj_ast_datatype, tj_ast_node_datatype, token.location,
                     {.type=type});
}

static tj_ast_node* parse_assign(tj_tokenize_state* state, tj_ast_node* name)
{
    token_get_and_expect(state, tj_token_equal, "expected '=' in assignment\n");
    tj_ast_node* value = tj_parse_expression(state);
    handle_errors_for_node(value);

    return make_node(tj_ast_assign, tj_ast_node_assign, value->location,
                     {.name=name, .value=value});
}

static tj_ast_node* parse_var_create(tj_tokenize_state* state)
{
    token_get_and_expect(state, tj_token_var, "expected 'var' at the beginning of variable creation\n");

    tj_ast_node* name = tj_parse_expression(state);
    handle_errors_for_node(name);
    
    tj_ast_node* type_hint = 0;
    if (!match(state, tj_token_equal))
    {
        type_hint = parse_datatype(state);
    }

    tj_ast_node* assign = parse_assign(state, name);
    handle_errors_for_node(assign);

    return make_node(tj_ast_var_create, tj_ast_node_var_create, assign->location,
                     {.assign=(tj_ast_assign*)assign, .type_hint=(tj_ast_datatype*)type_hint});
}

static tj_ast_node* parse_ret(tj_tokenize_state* state)
{
    token_get_and_expect(state, tj_token_ret, "expected 'ret' at the beginning of return statement\n");

    tj_ast_node* expr = tj_parse_expression(state);

    return make_node(tj_ast_ret, tj_ast_node_ret, expr->location,
                     {.expr=expr});
}

static tj_ast_node* parse_while(tj_tokenize_state* state)
{
    token_get_and_expect(state, tj_token_while, "expected 'while' at the beginning of while statement\n");

    tj_ast_node* condition = tj_parse_expression(state);
    handle_errors_for_node(condition);

    tj_ast_node* body = parse_body(state);
    handle_errors_for_node(body);

    return make_node(tj_ast_while, tj_ast_node_while, condition->location,
                     {.condition=condition, .body=(tj_ast_body*)body});
}

static tj_ast_node* parse_statement(tj_tokenize_state* state)
{
    tj_token token = tj_get_token(state);
    if (token.type == tj_token_eof) return node_eof(token);
    else tj_backtrack(state);

    tj_ast_node* node = 0;

    if (match(state, tj_token_var)) node = parse_var_create(state);
    else if (match(state, tj_token_if)) node = parse_if(state);
    else if (match(state, tj_token_ret)) node = parse_ret(state);
    else if (match(state, tj_token_while)) node = parse_while(state);
    else
    {
        node = tj_parse_expression(state);
        handle_errors_for_node(node);

        if (match(state, tj_token_equal)) node = parse_assign(state, node);
    }

    if (node->type != tj_ast_node_if &&
        node->type != tj_ast_node_while)
    {
        token_get_and_expect(state, tj_token_semicolon, "expected ';' at the end of a statement\n");
    }

    return node;
}

static tj_ast_node* parse_body(tj_tokenize_state* state)
{
    tj_token token = tj_get_token(state);
    handle_errors_for_token(token);
    token_expect(token, tj_token_curly_open, "expected '{' at the beginning of block\n");

    tj_ast_node_array arr;
    tj_init_dynamic_array(tj_ast_node*, arr);

    if (match(state, tj_token_curly_close)) 
    {
        tj_token tok = tj_get_token(state);
        return make_node(tj_ast_body, tj_ast_node_body, tok.location, {.body=arr});
    }

    do
    {
        tj_ast_node* node = parse_statement(state);
        handle_errors_for_node(node);

        tj_push(arr, node);
    } while (!match(state, tj_token_curly_close));
    tj_get_token(state);

    return make_node(tj_ast_body, tj_ast_node_body, token.location, {.body=arr});
}

static tj_ast_node* parse_proc(tj_tokenize_state* state)
{
    token_get_and_expect(state, tj_token_proc, "expected 'proc' at the beginning of a procedure\n");

    tj_token name_tok = tj_get_token(state);
    handle_errors_for_token(name_tok);

    tj_string_view name_val = tj_get_token_value(state->code, name_tok);
    token_expect(name_tok, tj_token_ident, "expected an identifier for procedure name, found '%.*s'\n",
                                             name_val.size, name_val.string);

    tj_ast_symbol* name = (tj_ast_symbol*)make_node(tj_ast_symbol, tj_ast_node_symbol, name_tok.location,
                                                        {.symbol=name_val});

    tj_ast_symbol_array arg_names;
    tj_init_dynamic_array(tj_ast_symbol*, arg_names);

    tj_ast_datatype_array arg_types;
    tj_init_dynamic_array(tj_ast_datatype*, arg_types);

    token_get_and_expect(state, tj_token_paren_open, "expected '(' at the beginning of parameter list\n");
    if (!match(state, tj_token_paren_close))
    {
        do
        {
            tj_token token = tj_get_token(state);
            handle_errors_for_token(token);

            tj_string_view val = tj_get_token_value(state->code, token);
            token_expect(token, tj_token_ident, "expected an identifier for argument name, found '%.*s'\n",
                                                  val.size, val.string);

            tj_push(arg_names, (tj_ast_symbol*)make_node(tj_ast_symbol, tj_ast_node_symbol, token.location,
                                                             {.symbol=val}));

            tj_ast_node* type = parse_datatype(state);
            handle_errors_for_node(type);
            tj_push(arg_types, (tj_ast_datatype*)type);
        } while (get_and_match(state, tj_token_comma));
        tj_backtrack(state);
    }
    token_get_and_expect(state, tj_token_paren_close, "expected ')' at the end of parameter list\n");

    tj_ast_node* return_type = parse_datatype(state);
    handle_errors_for_node(return_type);

    tj_ast_node* body = parse_body(state);
    handle_errors_for_node(body);

    return make_node(tj_ast_proc, tj_ast_node_proc, name_tok.location,
                     {.name=name, .parameters.names = arg_names, .parameters.types=arg_types,
                      .body=(tj_ast_body*)body, .return_type=(tj_ast_datatype*)return_type});
}

tj_ast_node* tj_parse_toplevel(tj_tokenize_state* state)
{
    if (match(state, tj_token_proc)) return parse_proc(state);

    return parse_statement(state);
}
