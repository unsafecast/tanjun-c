#ifndef tj_location_h
#define tj_location_h
#include <stdint.h>

typedef struct
{
    size_t start;
    size_t end;
} tj_location;

#define tj_make_location(_start, _end) ((tj_location){.start = (_start), .end = (_end)})

#endif