#ifndef tj_io_h
#define tj_io_h
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

char* tj_read_whole_file(const char* path);

#endif