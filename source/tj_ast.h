#ifndef tj_ast_h
#define tj_ast_h
#include <stdint.h>
#include "tj_token.h"
#include "tj_location.h"
#include "tj_string.h"
#include "tj_dynamic_array.h"
#include "tj_datatype.h"

typedef enum
{
    tj_ast_node_err = -99,

    tj_ast_node_none = 0,
    tj_ast_node_symbol,
    tj_ast_node_funcall,
    tj_ast_node_number,
    tj_ast_node_datatype,

    tj_ast_node_proc,
    tj_ast_node_body,
    tj_ast_node_assign,
    tj_ast_node_var_create,
    tj_ast_node_if,
    tj_ast_node_ret,
    tj_ast_node_while,

    tj_ast_node_eof,
} tj_ast_node_type;

typedef struct tj_ast_node
{
    tj_ast_node_type type;
    tj_location location;
} tj_ast_node;
typedef tj_dynamic_array(tj_ast_node*) tj_ast_node_array;

typedef struct
{
    tj_ast_node node;
    tj_string_view symbol;
} tj_ast_symbol;
typedef tj_dynamic_array(tj_ast_symbol*) tj_ast_symbol_array;

typedef struct
{
    tj_ast_node node;
    tj_string_view number;
} tj_ast_number;

typedef struct
{
    tj_ast_node node;
    tj_ast_node* name;

    tj_ast_node_array arguments;
} tj_ast_funcall;

typedef struct
{
    tj_ast_node node;
    tj_ast_node_array body;
} tj_ast_body;

typedef struct
{
    tj_ast_node node;
    tj_datatype* type;
} tj_ast_datatype;
typedef tj_dynamic_array(tj_ast_datatype*) tj_ast_datatype_array;

typedef struct
{
    tj_ast_node node;
    tj_ast_symbol* name;

    struct
    {
        tj_ast_symbol_array names;
        tj_ast_datatype_array types;
    } parameters;
    tj_ast_datatype* return_type;

    tj_ast_body* body;
} tj_ast_proc;

typedef struct
{
    tj_ast_node node;
    tj_ast_node* name;
    tj_ast_node* value;
} tj_ast_assign;

typedef struct
{
    tj_ast_node node;
    tj_ast_assign* assign;
    tj_ast_datatype* type_hint;  // NULL if none
} tj_ast_var_create;

typedef struct
{
    tj_ast_node node;
    tj_ast_node* condition;
    tj_ast_body* if_body;
    tj_ast_body* else_body;  // NULL if none
} tj_ast_if;

typedef struct
{
    tj_ast_node node;
    tj_ast_node* expr;
} tj_ast_ret;

typedef struct
{
    tj_ast_node node;
    tj_ast_node* condition;
    tj_ast_body* body;
} tj_ast_while;

#endif