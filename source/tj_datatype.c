#include "tj_datatype.h"

void tj_print_datatype(FILE* file, tj_datatype* type)
{
    switch (type->type)
    {
        case tj_datatype_type_none: fprintf(file, "<none>"); break;
        case tj_datatype_type_nothing: fprintf(file, "Nothing"); break;
        case tj_datatype_type_ptr:
        {
            tj_datatype_ptr* ptr = (tj_datatype_ptr*)type;
            fprintf(file, "*");
            tj_print_datatype(file, ptr->target);
        } break;
        case tj_datatype_type_int:
        {
            tj_datatype_int* i = (tj_datatype_int*)type;
            fprintf(file, "Int%d", i->bit_count);
        } break;
        case tj_datatype_type_user_defined:
        {
            tj_datatype_user_defined* user = (tj_datatype_user_defined*)type;
            fprintf(file, "%.*s", user->name.size, user->name.string);
        } break;
    }
}