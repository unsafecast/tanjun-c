#include "tj_string.h"

tj_string_view tj_slice(tj_string_view string, size_t start, size_t end)
{
    tj_string_view ret;
    ret.string = string.string + start;
    ret.size = end - start;

    return ret;
}

tj_string_view tj_slice_from(tj_string_view string, size_t from)
{
    return tj_slice(string, from, string.size - from);
}

bool tj_string_view_equal(tj_string_view string1, tj_string_view string2)
{
    if (string1.size != string2.size) return false;

    for (size_t i = 0; i < string1.size; i++)
    {
        if (string1.string[i] != string2.string[i]) return false;
    }

    return true;
}

void tj_print_string_view(FILE* file, tj_string_view string)
{
    fwrite(string.string, string.size, 1, file);
}
