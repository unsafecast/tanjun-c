#include "tj_tokenize.h"
#include "tj_string.h"
#include "tj_pretty.h"
#include "tj_ast.h"
#include "tj_parse.h"
#include "tj_io.h"

int main()
{
    tj_string_view code = tj_sv(tj_read_whole_file("examples/current_state.tj"));
    tj_tokenize_state tokenize_state = tj_make_tokenize_state(code);

    while (1)
    {
        tj_ast_node* node = tj_parse_toplevel(&tokenize_state);
        tj_pretty_ast_node(stdout, node, code, 0);
        fprintf(stdout, "\n");
        if (node->type == tj_ast_node_eof || node->type < 0) break;
    }

    return 0;
}
