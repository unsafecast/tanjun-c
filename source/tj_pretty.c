#include "tj_pretty.h"
#include "tj_tokenize.h"

static void print_indent(FILE* file, int indent_level)
{
    for (int i = 0; i < indent_level; i++)
    {
        fputs(" ", file);
    }
}

void tj_pretty_token(FILE* file, tj_token token, tj_string_view code)
{
    fputs("token(", file);

    switch (token.type)
    {
        case tj_token_err:
            fputs("error, ", file);
            break;

        case tj_token_none:
            fputs("none, ", file);
            break;
        case tj_token_ident:
            fputs("ident, ", file);
            break;
        case tj_token_number:
            fputs("number, ", file);
            break;
            
        case tj_token_paren_open:
            fputs("paren_open, ", file);
            break;
        case tj_token_paren_close:
            fputs("paren_close, ", file);
            break;
        case tj_token_comma:
            fputs("comma, ", file);
            break;
        case tj_token_curly_open:
            fputs("curly_open, ", file);
            break;
        case tj_token_curly_close:
            fputs("curly_close, ", file);
            break;
        case tj_token_semicolon:
            fputs("semicolon, ", file);
            break;
        case tj_token_star:
            fputs("star, ", file);
            break;
        case tj_token_equal:
            fputs("equal, ", file);
            break;

        case tj_token_proc:
            fputs("proc, ", file);
            break;
        case tj_token_var:
            fputs("var, ", file);
            break;
        case tj_token_if:
            fputs("if, ", file);
            break;
        case tj_token_else:
            fputs("else, ", file);
            break;
        case tj_token_while:
            fputs("while, ", file);
            break;
        case tj_token_ret:
            fputs("ret, ", file);
            break;

        case tj_token_eof:
            fputs("eof, ", file);
            break;
    }

    tj_string_view value = tj_get_token_value(code, token);
    fputs("'", file);
    tj_print_string_view(file, value);
    fputs("')", file);
}

void tj_pretty_ast_node(FILE* file, tj_ast_node* node, tj_string_view code, int indent_level)
{
    print_indent(file, indent_level);

    switch (node->type)
    {
        case tj_ast_node_err:
        {
            fputs("error", file);
        } break;

        case tj_ast_node_none:
            fputs("none", file);
            break;
        case tj_ast_node_symbol:
        {
            tj_ast_symbol* symbol = (tj_ast_symbol*)node;
            fputs("symbol(", file);
            tj_print_string_view(file, symbol->symbol);
            fputs(")", file);
        } break;
        case tj_ast_node_number:
        {
            tj_ast_number* number = (tj_ast_number*)node;
            fputs("number(", file);
            tj_print_string_view(file, number->number);
            fputs(")", file);
        } break;
        case tj_ast_node_funcall:
        {
            tj_ast_funcall* funcall = (tj_ast_funcall*)node;
            fputs("funcall(", file);
            tj_pretty_ast_node(file, funcall->name, code, 0);
            for (size_t i = 0; i < funcall->arguments.size; i++)
            {
                fputs(", ", file);
                tj_pretty_ast_node(file, funcall->arguments.elements[i], code, 0);
            }
            fputs(")", file);
        } break;

        case tj_ast_node_body:
        {
            tj_ast_body* body = (tj_ast_body*)node;
            fputs("\b\b", file); // This is because the first indent was already printed
                                 // at the beginning of the function
            for (size_t i = 0; i < body->body.size; i++)
            {
                tj_pretty_ast_node(file, body->body.elements[i], code, indent_level + 2);
                fputs("\n", file);
            }
        } break;
        case tj_ast_node_proc:
        {
            tj_ast_proc* proc = (tj_ast_proc*)node;
            fputs("proc(", file);
            tj_pretty_ast_node(file, (tj_ast_node*)proc->name, code, 0);
            for (size_t i = 0; i < proc->parameters.names.size; i++)
            {
                fputs(", ", file);
                tj_pretty_ast_node(file, (tj_ast_node*)proc->parameters.names.elements[i], code, 0);
                fputs(" ", file);
                tj_pretty_ast_node(file, (tj_ast_node*)proc->parameters.types.elements[i], code, 0);
            }
            fputs(") ", file);
            tj_pretty_ast_node(file, (tj_ast_node*)proc->return_type, code, 0);
            fputs("\n", file);
            tj_pretty_ast_node(file, (tj_ast_node*)proc->body, code, indent_level);
        } break;

        case tj_ast_node_datatype:
        {
            tj_ast_datatype* datatype = (tj_ast_datatype*)node;
            fputs("datatype(", file);
            tj_print_datatype(file, datatype->type);
            fputs(")", file);
        } break;

        case tj_ast_node_assign:
        {
            tj_ast_assign* assign = (tj_ast_assign*)node;
            fputs("assign(", file);
            tj_pretty_ast_node(file, (tj_ast_node*)assign->name, code, 0);
            fputs(" = ", file);
            tj_pretty_ast_node(file, assign->value, code, 0);
            fputs(")", file);
        } break;

        case tj_ast_node_var_create:
        {
            tj_ast_var_create* var_create = (tj_ast_var_create*)node;
            fputs("var_create(", file);
            tj_pretty_ast_node(file, (tj_ast_node*)var_create->assign, code, 0);
            if (var_create->type_hint != 0)
            {
                fputs(", ", file);
                tj_pretty_ast_node(file, (tj_ast_node*)var_create->type_hint, code, 0);
            }
            fputs(")", file);
        } break;

        case tj_ast_node_if:
        {
            tj_ast_if* if_node = (tj_ast_if*)node;
            fputs("if(", file);
            tj_pretty_ast_node(file, if_node->condition, code, 0);
            fputs(")\n", file);
            tj_pretty_ast_node(file, (tj_ast_node*)if_node->if_body, code, indent_level);

            if (if_node->else_body != 0)
            {
                print_indent(file, indent_level);
                fputs("else\n", file);
                tj_pretty_ast_node(file, (tj_ast_node*)if_node->else_body, code, indent_level);
            }
        } break;

        case tj_ast_node_while:
        {
            tj_ast_while* while_node = (tj_ast_while*)node;
            fputs("while(", file);
            tj_pretty_ast_node(file, while_node->condition, code, 0);
            fputs(")\n", file);
            tj_pretty_ast_node(file, (tj_ast_node*)while_node->body, code, indent_level);
        } break;

        case tj_ast_node_ret:
        {
            tj_ast_ret* ret = (tj_ast_ret*)node;
            fputs("ret(", file);
            tj_pretty_ast_node(file, ret->expr, code, 0);
            fputs(")", file);
        } break;

        case tj_ast_node_eof:
            fputs("eof", file);
            break;
    }
}
