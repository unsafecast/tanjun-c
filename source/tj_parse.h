#ifndef tj_parse_h
#define tj_parse_h
#include "tj_ast.h"
#include "tj_tokenize.h"

tj_ast_node* tj_parse_toplevel(tj_tokenize_state* state);
tj_ast_node* tj_parse_expression(tj_tokenize_state* state);

#endif