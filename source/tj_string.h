#ifndef tj_string_h
#define tj_string_h
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct
{
    char* string;
    size_t size;
} tj_string_view;

#define tj_sv(_arr) ((tj_string_view){.string = (_arr), .size = strlen((_arr))})
#define tj_make_string_view(_string, _size) ((tj_string_view){.string = (_arr), .size = (_size)})

tj_string_view tj_slice(tj_string_view string, size_t start, size_t end);
tj_string_view tj_slice_from(tj_string_view string, size_t from);
bool tj_string_view_equal(tj_string_view string1, tj_string_view string2);

void tj_print_string_view(FILE* file, tj_string_view string);

#endif