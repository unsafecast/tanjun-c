#ifndef tj_tokenize_h
#define tj_tokenize_h
#include <stdint.h>
#include "tj_string.h"
#include "tj_token.h"

typedef struct
{
    tj_string_view code;
    size_t position;
    size_t last_position;
} tj_tokenize_state;

#define tj_make_tokenize_state(_code) ((tj_tokenize_state){.code = (_code), .position = 0, .last_position = 0})

tj_token tj_get_token(tj_tokenize_state* state);
tj_string_view tj_get_token_value(tj_string_view code, tj_token token);

void tj_backtrack(tj_tokenize_state* state);

#endif