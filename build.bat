@echo off

rem the "<NUL set /p" thingy is like echo without newline

<NUL set /p=:: building tanjun
clang -Ofast -Wall -std=gnu2x -Wextra -D_CRT_SECURE_NO_WARNINGS -static -o tanjun.exe^
    source/tj_main.c^
    source/tj_string.c^
    source/tj_tokenize.c^
    source/tj_pretty.c^
    source/tj_parse.c^
    source/tj_io.c^
    source/tj_error.c^
	source/tj_datatype.c

if %errorlevel% neq 0 exit /b %errorlevel%
echo ...done

echo :: all build tasks done